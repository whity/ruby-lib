require( 'minds/utils/object/deep_clone' )

module Minds
    module Config
        class Hash < Plugin
            def initialize(data: {})
                @_data = ::Minds::Utils::Object.deep_clone( data )
            end

            def get(key)
                key = key.to_s
                
                value = self.to_h
                key.split('.').each do |item|
                    value = value[item]
                    if (!value.kind_of?(::Hash))
                        break
                    end
                end

                return value
            end

            def to_h()
                return ::Minds::Utils::Object.deep_clone(@_data)
            end
        end
    end
end
