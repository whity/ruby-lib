require('yaml')
require_relative('../backend')

module Minds
    class I18n
        module Backend
            class Yaml < Plugin
                #constructor
                def initialize(**kwargs)
                    @_data = Hash.new

                    #folder, move this to backend YAML
                    @_folder = kwargs[:folder]
                    if (!@_folder)
                        raise(ArgumentError, "missing argument 'folder'")
                    end
                    @_folder = (@_folder[-1] == '/') ? @_folder[0 .. -2] : @_folder

                    #load translations files
                    self.load

                    return self
                end

                def load()
                    @_data.clear

                    files = Dir.glob("#{ @_folder }/*.yml")
                    files.each do |file|
                        lang = ::YAML.load_file(file)
                        @_data.merge!(lang)
                    end

                    return self
                end
                
                #lookup for the key and return the value
                def lookup(locale, key, **options)
                    if (!@_data.has_key?(locale))
                        return
                    end

                    result = @_data[locale]
                    key.split('.') .each do |item|
                        result = result[item]
                        if (!result || !result.kind_of?(Hash))
                            break
                        end
                    end

                    return result
                end

                #return the available locales
                def available_locales()
                    return @_data.keys
                end
            end
        end
    end
end
