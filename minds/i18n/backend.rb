require( 'minds/pluggable' )

module Minds
    class I18n
        module Backend
            extend(::Minds::Pluggable)
            
            class Plugin
                #lookup for the key and return the value
                def lookup(locale, key, scope=[], options={})
                    raise(NotImplementedError)
                end

                #available locales
                def available_locales()
                    raise(NotImplementedError)
                end

                #load locales
                def load()
                    raise(NotImplementedError)
                end
                
                #return number formats
                def number(locale, **options)
                    return self.lookup(locale, '__formats__.number', **options)
                end

                #return currency formats
                def currency(locale, **options)
                    return self.lookup(locale, '__formats__.currency', **options)
                end

                #return date formats
                def date(locale, **options)
                    return self.lookup(locale, '__formats__.date', **options)
                end

                #return time formats
                def time(locale, **options)
                    return self.lookup(locale, '__formats__.time', **options)
                end
            end

            # setup plugins
            _pluggable_setup( require: false )

            class << self
                def create(type, **kwargs)
                    backend = self._pluggable_get(type)
                    if (!backend)
                        raise(ArgumentError, "invalid backend '#{ type }'")
                    end

                    return backend.new(**kwargs)
                end
            end
        end
    end
end
