require( 'minds/pluggable' )

module Minds
    module Config
        extend(::Minds::Pluggable)

        class Plugin
            def [](key)
                return self.get(key)
            end

            def []=(key, value)
                return self.set(key, value)
            end

            def get(key)
                raise(NotImplementedError)
            end

            def set(key, value)
                raise(NotImplementedError)
            end

            def to_h()
                raise(NotImplementedError)
            end
        end

        #setup plugins
        _pluggable_setup(require: false)

        class << self
            def create(type: nil, **args)
                config = self._pluggable_get(type)
                if (!config)
                    raise(ArgumentError, "invalid config type '#{ type }'")
                end

                return config.new(**args)
            end
        end
    end
end
