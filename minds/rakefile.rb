require( 'rake' )
require( 'rake/testtask' )

#set 't' as the working dir
Dir.chdir( File.dirname( File.expand_path( __FILE__ ) ) + "/t" )

###build lib path
lib_path = Dir.getwd + '/lib'

#add lib_path to LOAD_PATH
$LOAD_PATH.unshift( lib_path )

Rake::TestTask.new do |t|
    t.name       = :default
    t.test_files = FileList['**/*_t.rb']
end
