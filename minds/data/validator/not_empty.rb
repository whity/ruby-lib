module Minds
    module Data
        module Validator
            class NotEmpty < Plugin
                def self.process( value, kwargs={} )
                    if  ( value == nil )
                        return false
                    end

                    if ( value.kind_of?( Numeric ) )
                        value = value.to_s
                    end

                    if ( value.length > 0 )
                        return true
                    end

                    return false
                end
            end
        end
    end
end
