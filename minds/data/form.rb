require( 'minds/utils/object/deep_clone' )
require( 'minds/data/validator' )

module Minds
    module Data
        class Form
            
            class Field
                def initialize( **attrs )
                    @_attributes = attrs
                end

                def method_missing( method, *args, &block )
                    # if last char is '=' it's a set
                    if ( method.to_s[-1] == '=' )
                        method = method.to_s[0 .. -2].to_sym
                        @_attributes[method] = args[0]
                    end 

                    if ( @_attributes.has_key?( method ) )
                        return @_attributes[method]
                    end

                    return nil
                end
            end

            class << self
                def inherited( cls )
                    super( cls )

                    cls.send( :_metaclass ).class_variable_set( :@@_fields, {} )
                    
                    return
                end

                def validators_module()
                    return ::Minds::Data::Validator
                end

                # PROTECTED METHODS #
                protected

                def _field( **kwargs )
                    name   = kwargs[:name]
                    fields = self._metaclass.class_variable_get( :@@_fields )
                    fields[name] = Field.new( **kwargs )

                    return self
                end

                def _metaclass()
                    return class << self; self; end
                end
            end
            
            def initialize( &block )
                @_fields = Hash.new
                
                metaclass = self.class.send( :_metaclass )

                if ( metaclass.class_variable_defined?( :@@_fields ) )
                    @_fields = ::Minds::Utils::Object.deep_clone( 
                        metaclass.class_variable_get( :@@_fields ) 
                    )
                end

                if ( block != nil )
                    block.call( self )
                end

                return self
            end

            def add_field( **kwargs )
                name = kwargs[:name]
                @_fields[name] = Field.new( **kwargs )

                return self
            end
            
            def data=( data={} )
                data.each do |name, value|
                    if ( @_fields.has_key?( name ) )
                        @_fields[name].value = value
                    end
                end
                
                return self
            end

            def data()
                values = Hash.new
                @_fields.each do |name, field|
                    values[name] = field.value
                end

                return values
            end

            def validate()
                validators_module = self.class.validators_module
                errors = Hash.new

                @_fields.each do |name, field|
                    validators = field.validators
                    result     = validators_module.process( 
                        field.value,
                        :validators => field.validators,
                        :context    => self
                    )

                    if ( result )
                        errors[name] = result
                    end
                end
                
                if ( errors.empty? )
                    return
                end

                return errors
            end

            def method_missing( method, *args, &block )
                if ( @_fields.has_key?( method.to_s ) )
                    return @_fields[method.to_s]
                end

                return super( method, *args, &block )
            end
        end
    end
end
