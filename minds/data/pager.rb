module Minds
    module Data
        class Pager
            attr_reader(
                :total_entries, 
                :entries_per_page, 
                :current_page, 
                :last_page, 
                :previous_page, 
                :next_page,
                :pages
            )

            def initialize(total_entries, entries_per_page, current_page, **kwargs)
                @total_entries = total_entries
                @entries_per_page = entries_per_page
                @current_page = current_page

                #calculate last page
                @last_page = (@total_entries.to_f / @entries_per_page).ceil

                #available pages interval default
                @_interval = kwargs[:interval]
                if (!@_interval)
                    @_interval = 10
                end

                #calculate next page and previous page
                @previous_page = @current_page > 1 ? @current_page - 1 : 1
                @next_page = @current_page < @last_page ? @current_page + 1 : @last_page

                #calculate the pages
                @pages = self._pages
            end
            
            # Protected methods
            protected

            def _pages()
                #half pages
                half_pages = @_interval / 2

                #start page
                start_page = @current_page - half_pages

                #ajust start page if smaller than 1
                if (start_page < 1)
                    start_page = 1
                end

                #calc end page
                end_page = start_page + @_interval - 1

                #Decrease end to the maximum number of pages, if needed
                diff = 0
                while (end_page > @last_page) do
                   end_page -= 1
                   diff += 1
                end

                #Decrease start to include difference from end to number of pages
                counter = 0;
                while (start_page > 1 && counter < diff) do
                    start_page -= 1
                    counter += 1
                end

                return (start_page .. end_page).to_a
            end
        end
    end
end
