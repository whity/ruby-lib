require( 'minds/pluggable' )
require( 'minds/utils/object/deep_clone' )

module Minds
    module Data
        module Validator
            extend(::Minds::Pluggable)

            module Exception
                class InvalidValidatorException < ::Exception
                    def initialize(validator)
                        super("invalid validator '#{ validator }'")
                        return self
                    end
                end
            end

            module Methods
                def extended(othermod)
                    super
                    
                    #extend base mod with pluggable and methods
                    othermod.extend(::Minds::Pluggable)
                    othermod.extend(Methods)
                    
                    #copy pluggable config 
                    othermod.class_variable_set(
                        :@@_pluggable, 
                        ::Minds::Utils::Object.deep_clone( self.class_variable_get( :@@_pluggable ) )
                    )

                    return othermod
                end

                #if we get here means the method doesn't exists, process a validator
                def method_missing(sym, *args, &block)
                    begin
                        # process
                        return self._pluggable_get( sym ).process( *args )
                    rescue Minds::Pluggable::Exception::InvalidPluginException
                        raise(Exception::InvalidValidatorException, sym)
                    end
                end

                def process( value, **kwargs )
                    validators = kwargs.delete( :validators )

                    #check value against all passed validators, stop on first failure
                    # the return value should be the failed validator
                    validators.each do |item|
                        validator = item
                        args      = { :context => kwargs[:context] }
                        
                        if ( item.kind_of?( Array ) )
                            validator   = item[0]
                            args[:args] = item[1 .. -1]
                        end
                        
                        if ( !self.send( validator.to_sym, value, args ) )
                            return validator
                        end
                    end

                    return
                end

                def use(name)
                    begin
                        self._pluggable_get(name)
                    rescue Minds::Pluggable::Exception::InvalidPluginException => ex
                        raise(Exception::InvalidValidatorException, name)
                    end

                    return
                end
            end

            extend( Methods )

            class Plugin
                def self.process()
                    raise( NotImplementedError )
                end
            end
            
            _pluggable_setup
        end
    end
end
