module Minds
    module Utils
        module Object
            class << self
                def deep_clone( obj )
                    dump    = Marshal.dump( obj )
                    new_obj = Marshal.load( dump )

                    return new_obj
                end
            end
        end
    end
end
