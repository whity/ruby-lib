module Minds
    module Array
        module Filter
            class Process
                def initialize()
                    @_item_type = nil
                end

                def run(list, clauses=[])
                    #check clauses and list type
                    {:clauses => clauses, :list => list}.each do |arg, value|
                        if (!value.kind_of?(::Array))
                            raise(ArgumentError, "'#{ arg.to_s }': expecting an array object")
                        end
                    end

                    #for each item validate conditions
                    items = list.select do |item|
                        self._validate_item(item, clauses)
                    end

                    return items
                end
                
                # Protected methods               
                protected

                def _validate_item(item, clauses, and_or=:and)
                    #validate item with clauses
                    valid = false

                    catch(:check) do
                        clauses.each_with_index do |clause, idx|
                            clause.each do |key, value| #this could be a hash with multiple clauses
                                valid_clause = false

                                if (key.match(/^\-(and|or)$/i))
                                    valid_clause = self._validate_item(item, value, key[1..-1].to_sym)
                                else
                                    clause_obj = value

                                    if (!clause_obj.kind_of?(Clause::Plugin))
                                        if (key.match(/^\-code$/)) #it's a block clause
                                            clause_obj = Clause::Block.new(self, &value)
                                        else
                                            #simple clause, build it
                                            oper = '='
                                            vl = value
                                            if (value.kind_of?(Hash))
                                                tmp_value = value.to_a()[0]
                                                oper = tmp_value[0]
                                                vl = tmp_value[1]
                                            end
                                            
                                            clause_obj = Clause::Simple.new(self, key, Operator.create(oper), vl)
                                        end

                                        clauses[idx][key] = clause_obj
                                    end

                                    #get item column value to check
                                    item_value = item
                                    if (clause_obj.kind_of?(Clause::Plugin))
                                        item_value = self._item_column_value(item, clause_obj.column)
                                    end

                                    #check item against clause
                                    valid_clause = clause_obj.check(item_value)
                                end

                                if (and_or == :and)
                                    if (!valid_clause)
                                        #stop checking, in AND all clauses must be true, one fails, item invalid
                                        valid = false
                                        throw(:check)
                                    else
                                        valid = true
                                    end
                                elsif (valid_clause && and_or == :or)
                                    #stop cheking, in OR only needs one clause valid
                                    valid = true
                                    throw(:check)
                                end
                            end

                        end
                    end

                    return valid
                end

                def _item_column_value(item, column)
                    if (!@_item_type)
                        #guess item type
                        if (item.kind_of?(Hash)) #if item is kind of hash
                            @_item_type = lambda { |item, column| return item[column] }
                        else
                            @_item_type = lambda { |item, column| return item.send(column) }
                        end
                    end
                    
                    return @_item_type.call(item, column)
                end
            end #search
        end
    end
end
