module Minds
    module Array
        module Filter
            module Operator
                extend(::Minds::Pluggable)

                class Plugin
                    class << self
                        def symbol()
                            return self.class_variable_get(:@@_symbol)
                        end

                        # Protected methods
                        protected

                        def symbol=(value)
                            value = value.to_sym
                            self.class_variable_set(:@@_symbol, value)

                            return value
                        end
                    end

                    def call()
                        raise(NotImplementedError)
                    end
                end

                class << self
                    def create(operator, *args)
                        operator = _pluggable_get(operator)
                        return operator.new(*args)
                    end

                    def register(cls)
                        #register the plugin
                        plugin = _pluggable_register_plugin(cls)

                        #replace the 'names' by the symbol
                        plugin[:names].each do |name|
                            @@_pluggable[:plugins_names].delete(name.to_sym)
                        end
                        @@_pluggable[:plugins_names][plugin[:object].symbol] = plugin[:names][0].to_sym

                        return self
                    end
                end

                #load plugins and register them
                _pluggable_setup(:require => true)

                #change 'plugins_names' to symbol => name
                operators_names = ::Hash.new
                @@_pluggable[:plugins_names].each do |key, operator|
                    if (operators_names.has_key?(operator))
                        next
                    end

                    operator_obj = _pluggable_get(operator)
                    operators_names[operator_obj.symbol] = operator
                end
                @@_pluggable[:plugins_names] = operators_names

            end #operator
        end
    end
end
