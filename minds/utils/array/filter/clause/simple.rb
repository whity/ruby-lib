module Minds
    module Array
        module Filter
            module Clause
                class Simple < Plugin
                    attr_reader(:column)

                    def initialize(search, column, operator, value) #constructor
                        @_search = search
                        @column = column.to_sym
                        @_operator = operator
                        @_value = value
                    end

                    def check(item_value)
                        return @_operator.call(item_value, *@_value)
                    end
                end
            end
        end
    end
end
