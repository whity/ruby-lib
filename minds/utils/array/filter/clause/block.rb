module Minds
    module Array
        module Filter
            module Clause
                class Block < Plugin
                    #constructor
                    def initialize(search, &block)
                        @_search = search
                        @_block = block
                    end

                    def check(item)
                        return @_block.call(item)
                    end
                end
            end
        end
    end
end
