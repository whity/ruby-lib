require( 'minds/pluggable' )

module Minds
    module Array
        module Filter
            module Clause
                extend( ::Minds::Pluggable )
            
                class Plugin
                    def check( item_value )
                        raise( NotImplementedError )
                    end
                end

                _pluggable_setup( :require => true )
            end
        end
    end
end
