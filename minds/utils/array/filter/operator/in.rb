module Minds
    module Array
        module Filter
            module Operator
                class In < Plugin
                    self.symbol = 'in'

                    def call(item_value, *values)
                        #checks if item_value exists in the array
                        if (values.respond_to?('include?'.to_sym) && values.include?(item_value))
                            return true
                        end

                        return false
                    end
                end
            end
        end
    end
end
