module Minds
    module Array
        module Filter
            module Operator
                class Less < Plugin
                    self.symbol = '<'

                    def call(*values)
                        (item_value, value) = values
                        if (item_value < value)
                            return true
                        end

                        return false
                    end
                end
            end
        end
    end
end
