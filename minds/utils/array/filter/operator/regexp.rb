module Minds
    module Array
        module Filter
            module Operator
                class Regexp < Plugin
                    self.symbol = '=~'

                    def call(*values)
                        (item_value, value) = values
                        if (value.match(item_value.to_s))
                            return true
                        end

                        return false
                    end
                end
            end
        end
    end
end
