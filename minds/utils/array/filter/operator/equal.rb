module Minds
    module Array
        module Filter
            module Operator
                class Equal < Plugin
                    self.symbol = '='

                    def call(*values)
                        (item_value, value) = values
                        if (value == item_value)
                            return true
                        end

                        return false
                    end
                end
            end
        end
    end
end
