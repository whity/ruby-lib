require_relative( 'filter/process')
require_relative( 'filter/clause')
require_relative( 'filter/operator')

module Minds
    module Utils
        module Array
            class << self
                def filter( obj, *clauses )
                    return ::Minds::Array::Filter::Process.new.run( obj, clauses )
                end
            end
        end
    end
end
