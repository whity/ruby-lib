require_relative( "sort/exceptions" )
require_relative( "sort/expression" )
require_relative( "sort/process" )

module Minds
    module Utils
        module Array
            class << self
                def sort(obj, *conditions)
                    return ::Minds::Utils::Array::Sort::Process.new.run(obj, *conditions)
                end
            end
        end
    end
end
