module Minds
    module Utils
        module Array
            module Sort
                module Expression
                    class Simple < Plugin
                        attr_accessor(:column, :direction)
                        
                        #contructor
                        def initialize(column, direction)
                            @column = column
                            @direction = direction
                        end
                        
                        def to_s()
                            return "#{ @column } - #{ @_direction.to_s }"
                        end
                    end
                end
            end
        end
    end
end
