module Minds
    module Utils
        module Array
            module Sort
                module Expression
                    extend(::Minds::Pluggable)
                
                    class Plugin
                    end

                    class << self
                        def get(plugin=nil)
                            return _pluggable_get(plugin)
                        end
                    end

                    _pluggable_setup(:require => true)
                end
            end
        end
    end
end
