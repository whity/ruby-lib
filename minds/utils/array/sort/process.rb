module Minds
    module Utils
        module Array
            module Sort
                class Process
                    @@_block_cls = Expression.get(:Block)

                    def initialize()
                        @_item_type = nil
                    end

                    def run(list, *conditions)
                        #parse expressions
                        expressions = self._parse(conditions)

                        #sort items
                        sorted = list.sort { |a,b| self._cmp(a, b, expressions) }

                        return sorted
                    end

                    # protected methods
                    protected

                    def _cmp(a, b, expressions)
                        #compare each expression with the item values
                        compare_result = 0 #by default, the items values are equal
                        expressions.each do |expr|
                            if (expr.kind_of?(@@_block_cls))
                                compare_result = expr.block.call(a,b)
                            else
                                cmp_objects = {
                                    :a => {:obj => a, :value => nil},
                                    :b => {:obj => b, :value => nil}
                                }
                                
                                cmp_objects.each do |key, value|
                                    value[:value] = self._item_column_value(value[:obj], expr.column)
                                end
                                
                                #compare values
                                compare_result = cmp_objects[:a][:value] <=> cmp_objects[:b][:value]
                                if (compare_result != 0) #not equal check the direction
                                    if (expr.direction == :desc) #descent sort
                                        compare_result = (compare_result == 1) ? -1 : 1
                                    end
                                end 
                            end
                            
                            #if item values are different, stop expr processing
                            if (compare_result != 0)
                                break
                            end  
                        end
                        
                        return compare_result
                    end

                    def _parse(conditions)
                        expressions = ::Array.new
                        
                        conditions.each do |cond|
                            if (cond.kind_of?(String))
                                #split sort by ',' and build expression objects
                                sort_list = cond.split(',')
                                sort_list.each do |expr|
                                    expr_splitted = expr.strip.split(' ')
                                    
                                    if (expr_splitted.empty?)
                                        next #no expression, continue to the next one
                                    end
                                    
                                    #direction
                                    direction = :asc #by default use ascending
                                    if (expr_splitted.size > 1)
                                        direction = expr_splitted[1].to_sym
                                    end
                                    
                                    expr_obj = Expression.get(:Simple).new(expr_splitted[0].to_sym, direction)
                                    expressions.push(expr_obj)
                                end
                            elsif (cond.kind_of?(Proc))
                                expressions.push(@@_block_cls.new(cond))
                            else
                                raise(Exception::InvalidConditionException, "invalid condition: #{ cond.to_s }")
                            end
                        end
                        
                        return expressions
                    end

                    def _item_column_value(item, column)
                        if (!@_item_type)
                            #guess item type
                            if (item.kind_of?(::Hash)) #if item is kind of hash
                                @_item_type = lambda { |item, column| return item[column] }
                            else
                                @_item_type = lambda { |item, column| return item.send(column) }
                            end
                        end
                        
                        return @_item_type.call(item, column)
                    end
                end
            end
        end
    end
end
