module Minds
    module Utils
        module Hash
            class << self
                def symbolize_keys( obj, deep: false )
                    new_hash = ::Hash.new

                    obj.each do |key, value|
                        new_value = value
                        if (deep == true && new_value.kind_of?(Hash))
                            new_value = ::Minds::Utils::Hash.symbolize_keys(obj, :deep => deep)
                        end

                        new_hash[key.to_sym] = new_value
                    end

                    return new_hash
                end
            end
        end
    end
end
