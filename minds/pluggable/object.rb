module Minds
    module Pluggable
        class Object
            module Exception
                class InvalidPluginException < ::Exception
                end
            end
            
            attr_reader( :config )

            def initialize( **config )
                # check for namespace
                if ( !config[:namespace] )
                    raise( ::ArgumentError, "'namespace' argument is missing" )
                end

                @config         = config
                @_plugins       = Hash.new
                @_plugins_names = Hash.new

                # based_on, copy all the plugins
                based_on = config.delete( :based_on )
                if ( based_on )
                    @_plugins       = based_on.instance_variable_get( :@_plugins ).clone
                    @_plugins_names = based_on.instance_variable_get( :@_plugins_names ).clone
                end

                # plugins directory
                namespace_path      = config[:namespace].to_s.downcase.gsub( '::', '/' )
                full_namespace_path = nil
                available_paths = [ '.' ].concat( $LOAD_PATH )
                available_paths.each do |path|
                    full_path = File.absolute_path( path + '/' + namespace_path )
                    if ( File.directory?( full_path ) )
                        full_namespace_path = full_path
                        break
                    end
                end

                if ( full_namespace_path )
                    # get the plugins
                    self._load_plugins( full_namespace_path )
                end

                return self
            end
            
            def plugin( plugin )
                plugin = plugin.to_sym
                
                # check if plugin is already loaded, if not, load it
                if ( !self._plugin_loaded?( plugin ) )
                    plugin = @_plugins_names[plugin]
                    self._load_plugin( @_plugins[plugin] )
                else # plugin already loaded
                    plugin = @_plugins_names[plugin]
                end
                
                return @_plugins[plugin][:object]
            end

            def plugins()
                return @_plugins.keys
            end

            def register_plugin( cls, **kwargs )
                # => get the class name
                # => get the namespace
                # => build the alternative names

                namespace = cls.to_s.sub( /^::/i, '' ) # remove starting separator, if exists
                namespace = namespace.split( '::' ) # split by '::'
                name      = namespace.pop # the name is the last position of namespace
                namespace = Kernel.const_get( namespace.join( "::" ) )

                names = kwargs[:names]
                if ( !names )
                    names = self._plugin_names( [name] )
                end

                # ensure all names are symbols
                names = names.map { |item| item.to_sym }

                plugin = {
                    :names     => names,
                    :namespace => namespace,
                    :object    => cls,
                    :filename  => nil
                }
                
                self._plugin_register( plugin )

                return plugin[:object]
            end


            # PROTECTED METHODS
            protected

            def _plugin_register( plugin )
                names = plugin[:names]

                # first name is the pointer
                name = names[0]

                # register plugin and names
                @_plugins[name] = plugin
                names.each do |item|
                    @_plugins_names[item] = name
                end
            end

            def _load_plugins( dir, parent=[] )
                dir = Dir.new( dir )
                dir.each do |item|
                    #discard items that start with dot ( . )
                    if (item[0] == '.')
                        next
                    end

                    full_item_path = dir.path + '/' + item
                    if ( File.directory?( full_item_path ) )
                        self._load_plugins( full_item_path, parent + [item] )
                    elsif ( File.extname( item ) == '.rb' )
                        # generate all possible names for the plugin
                        # save the object, to avoid searching when requesting it, make it on _load_plugin
                        plugin_names = self._plugin_names( parent + [item] )
                        plugin = {
                            :names     => plugin_names,
                            :filename  => full_item_path,
                            :namespace => @config[:namespace],
                            :object    => nil,
                        }
                        
                        register = true
                        if ( @config[:require] )
                            begin
                                self._load_plugin( plugin )
                            rescue Exception::InvalidPluginException => ex
                                # don't register plugin
                                register = false
                            end
                        end

                        if ( register )
                            self._plugin_register( plugin )
                        end
                    end
                end

                return
            end

            def _plugin_names( item )
                file = item.shift

                # remove extension
                file.sub!( /\.\w+$/i, '' )

                ### name possibilities 
                names = [file.upcase.to_sym, file.downcase.to_sym]

                # capitalize
                file_split = file.split( /_/ )
                file_split.map! { |it| it.capitalize }
                names.unshift( file_split.join( '' ).to_sym )
                ###

                result = names
                if ( item.length > 0 )
                    result = ::Array.new
                    restant_names = self._plugin_names( item.clone )
                    names.each do |name|
                        restant_names.each do |sub_name|
                            result.push( "#{ name }::#{ sub_name }".to_sym )
                        end
                    end
                end

                return result
            end

            def _load_plugin( plugin )
                # if plugin already loaded, don't do nothing
                if ( plugin[:object] != nil )
                    return
                end

                # require file
                require( plugin[:filename] )
                
                # get plugin namespace
                if ( plugin[:namespace].kind_of?( String ) )
                    plugin[:namespace]  = Kernel.const_get( plugin[:namespace] )
                    @config[:namespace] = plugin[:namespace]
                end

                # search for the object
                names = plugin[:names]
                names.each do |name|
                    # skip name that's all in lower case
                    if ( name.downcase == name )
                        next
                    end

                    tmp_object = plugin[:namespace]
                    name.to_s.split( '::' ).each do |item|
                        if ( tmp_object.const_defined?( item ) )
                            tmp_object = tmp_object.const_get( item )
                        else
                            tmp_object = nil
                            break
                        end
                    end

                    # if tmp_object isn't nil, indicates that we found the object, stop names loop
                    if ( tmp_object )
                        plugin[:object] = tmp_object
                        break
                    end
                end

                if ( plugin[:object] == nil )
                    raise( Exception::InvalidPluginException, "invalid plugin: #{ names.join( ',' ) }" )
                end

                return
            end

            def _plugin_loaded?( plugin )
                # get plugin key/name
                if ( !self._plugin_exist?( plugin ) )
                    raise( Exception::InvalidPluginException, "invalid plugin '#{ plugin }'" )
                end

                name = @_plugins_names[plugin.to_sym]
                if ( @_plugins[name][:object] )
                    return true
                end

                return false
            end

            def _plugin_exist?( plugin )
                if ( plugin == nil )
                    raise( ArgumentError, "plugin name not provided" )
                end

                return @_plugins_names.has_key?( plugin.to_sym )
            end
        end
    end
end
