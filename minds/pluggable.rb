require( 'minds/pluggable/object' )

module Minds
    module Pluggable
        # Protected methods
        protected

        def _pluggable_check_setup()
            #ensure we've done setup
            if ( !self.class_variable_defined?( :@@_pluggable ) )
                raise( ::Exception, 'pluggable not configured' )
            end

            return
        end

        def _pluggable_setup( **config )
            if ( self.class_variable_defined?( :@@_pluggable ) )
                tmp_config = self.class_variable_get( :@@_pluggable ).config
                config = tmp_config.merge( config )
            end

            if ( !config[:namespace] )
                config[:namespace] = self
            end
            
            pluggable = ::Minds::Pluggable::Object.new( **config )

            self.class_variable_set( :@@_pluggable, pluggable )
        end

        def _pluggable_get( plugin=nil )
            self._pluggable_check_setup

            pluggable = self.class_variable_get( :@@_pluggable )

            # if plugin not provided get all
            if ( plugin == nil )
                return pluggable.plugins.keys
            end
            
            return pluggable.plugin( plugin )
        end
        
        def _pluggable_register_plugin( cls, **kwargs )
            self._pluggable_check_setup

            pluggable = self.class_variable_get( :@@_pluggable )

            return pluggable.register_plugin( cls, **kwargs )
        end
    end
end
