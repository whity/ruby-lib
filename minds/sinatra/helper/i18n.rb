module Minds
    module Sinatra
        module Helper
            module I18n
                def i18n()
                    return request.env['i18n']
                end
            end
        end
    end
end
