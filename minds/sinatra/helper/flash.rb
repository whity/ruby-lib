module Minds
    module Sinatra
        module Helper
            module Flash
                def flash()
                    return request.env['rack.session.flash']
                end
            end
        end
    end
end
