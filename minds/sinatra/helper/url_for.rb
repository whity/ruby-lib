module Minds
    module Sinatra
        module Helper
            module UrlFor
                def url_for( path, kwargs={} )
                    kwargs[:query] = kwargs[:query] || {}

                    url = path

                    # if path starts with slash is full, otherwise is relative
                    if ( path[0] == '/' )
                        port = request.port
                        if ( ( request.secure? && port == 443 ) || port == 80 )
                            port = nil
                        end

                        url  = "#{ request.scheme }://#{ request.host }#{ port ? ':' + port.to_s : '' }#{ url }"
                    end
                    
                    # querystring
                    url = self._url_querystring( url, kwargs[:query] )

                    return url
                end
                
                # PROTECTED METHODS
                protected

                def _url_querystring( url, querystring )
                    result = Array.new
                    querystring.each do |key, value|
                        if ( !value.kind_of?( Array ) )
                            value = [value]
                        end

                        value = value.map { |vl| "#{ key.to_s }=#{ vl }" }
                        
                        result.concat( value )
                    end

                    if ( result.length > 0 )
                        url += '?' + result.join( '&' )
                    end

                    return url
                end
            end
        end
    end
end
