module Minds
    module Sinatra
        module Extension
            module MountPoint
                class << self
                    # init controller mountpoint when the extension is registered
                    def extended(base)
                        super(base)

                        name = (base.name.split('::')[-1]).downcase
                        base.mount_point = '/' + name
                        base.mountable = true

                        return
                    end
                end

                def inherited(subclass)
                    super(subclass)

                    # init subclass mount_point
                    mount_point = self.mount_point
                    if (mount_point == nil)
                        mount_point = ''
                    end

                    # remove ending slash
                    mount_point = mount_point.sub( /\/?$/, '' )

                    name = (subclass.name.split('::')[-1]).downcase
                    subclass.mount_point = mount_point + '/' + name
                    subclass.mountable = true

                    return
                end

                def mount_point=(value)
                    self._metaclass.class_variable_set(:@@mount_point, value)
                end

                def mount_point()
                    metaclass = self._metaclass
                    if (!metaclass.class_variable_defined?(:@@mount_point))
                        return
                    end

                    return metaclass.class_variable_get(:@@mount_point)
                end

                def mountable=(value)
                    self._metaclass.class_variable_set(:@@mountable, value)
                end

                def mountable?()
                    metaclass = self._metaclass
                    if (!metaclass.class_variable_defined?(:@@mountable))
                        return false
                    end

                    return metaclass.class_variable_get(:@@mountable)
                end

                # Protected methods
                protected

                def _metaclass()
                    return class << self; self; end
                end
            end
        end
    end
end
