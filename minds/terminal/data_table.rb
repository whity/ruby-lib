require('highline')

module Minds
    module Terminal
        class DataTable
            #constructor
            def initialize(columns, data)
                @_columns = columns
                @_data = data
            end
            
            def build()
                ###build data structure###
                data = Array.new
                
                #fix the columns width according to the terminal/console width
                self._fix_cols_width

                #build header
                data.push(self._header_row)
                
                #build data
                data.concat(self._data_rows)
                ###
                
                ###build text table
                return self._build_output(data)
            end
            
            # Protected methods
            protected

            def _header_row()
                #build header row
                row = Hash.new
                @_columns.each do |col|
                    row[col['name']] = col['label']
                end
                
                return row
            end
        
            def _data_rows()
                lines = Array.new 
                
                @_data.each do |row|
                    data_row = Hash.new
                    @_columns.each() do |col|
                        value = nil
                        begin
                            value = row.send(col['name'].to_sym)
                        rescue
                            value = row[col['name']]
                        end
                        
                        data_row[col['name']] = value
                    end
                    
                    lines.push(data_row)
                end
                
                return lines
            end
            
            def _build_output(data)
                def _row_separator()
                    output = "+"
                    @_columns.each do |col|
                        output += "".ljust(col['chars'], "-")
                    end
                    output += "".ljust(@_columns.length - 1, "-") + "+"
                    
                    return output
                end
                
                def _row_lines(row, initial_data_row)
                    lines = Array.new
                    
                    #check for each column value how much lines it will use
                    @_columns.each do |col|
                        line = 0
                        value = row[col['name']].to_s
                        while (value && value.length > 0)
                            tmp_value = value[0 .. col['chars'] - 1]
                            line_data = initial_data_row.clone
                            
                            if (line >= lines.length)
                                lines.push(line_data)
                            else
                                line_data = lines[line]
                            end
                            
                            line_data[col['name']] = tmp_value.ljust(col['chars'], " ")
                            value = value[col['chars'] .. -1]
                            
                            line += 1
                        end
                    end
                    
                    #build output
                    output = ""
                    lines.each do |line|
                        output += "\n"
                        line.each do |k,v|
                            output += "|#{ v }"
                        end
                        output += "|"
                    end
                    
                    return output
                end
                
                #row separator
                row_separator = _row_separator
                
                #initial data row
                initial_data_row = Hash.new
                @_columns.each do |col|
                    initial_data_row[col['name']] = "".ljust(col['chars'], " ")
                end
                
                output = row_separator
                
                #process data
                data.each do |row|
                    output += _row_lines(row, initial_data_row)
                    output += "\n#{ row_separator }"
                end
                
                return output
            end

            def _fix_cols_width()
                #get console width and height
                (width, height) = HighLine::SystemExtensions.terminal_size
                width -= 5

                #calculate width provided by columns
                cols_width = 0
                @_columns.each do |col|
                    cols_width += col['chars']
                end

                #auto resize cols width
                new_width = 0
                @_columns.each do |col|
                    col_width_percentage = ((col['chars'] * 100.0) / cols_width).to_i
                    col['chars'] = ((col_width_percentage * width) / 100.0).to_i
                    new_width += col['chars']
                end

                return
            end
        end
    end
end
