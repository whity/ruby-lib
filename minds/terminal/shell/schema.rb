require( 'minds/schema' )

module Minds
    module Terminal
        class Shell
            class Schema < Minds::Schema
                def self.search_dirs()
                    return [File.expand_path(__FILE__).gsub(/\.\w*$/i, '')]
                end
            end
        end
    end
end 
