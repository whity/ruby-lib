class Command < Sequel::Model(:tb_commands)
    def self.__setup__(namespace)
        #create table if doesn't exist
        if (!db.tables.include?(:tb_commands))
            db.create_table(:tb_commands) do
                primary_key(:id, :type => Bignum)
                String(:command, :size => 500)
                DateTime(:datetime, :index => true)
            end
        end

        return nil
    end
end
