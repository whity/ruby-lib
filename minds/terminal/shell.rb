require(' singleton' )
require( 'readline' )
require( 'minds/app' )

module Minds
    module Terminal
        class Shell < App
            include( Singleton )

            class CommandScript
                #constructor
                def initialize(file)
                    contents = File.readlines(file)
                    contents = contents.join('')
                    self.instance_eval(contents)
                end
            end

            class << self
                #check method on application instance
                def method_missing(meth, *args, &block)
                    return self.instance.send(meth.to_sym, *args, &block)
                end

                def add_command(name, &block)
                    if (!block)
                        raise(ArgumentError, "block code not given")
                    end

                    method_name = "_cmd_#{ name }".to_sym
                    self.class_eval do
                        define_method(method_name, &block)
                    end
                    protected(method_name)
                end
            end

            #constructor
            def initialize()
                super

                #root app folder
                @_root = self._root

                #shell identifier
                @_identifier = self._identifier

                #array that will store the commands history to save before exit
                @_cmd_history = Array.new

                #history schema
                @_history = Schema.new(:dsn => "sqlite://#{ @_root }/.shell_history.db")

                #load history
                self._load_history

                #default commands folder
                @_cmd_folder = @_root + '/commands'

                @_started = false
            end

            def started?()
                return @_started
            end

            def start(kwargs={})
                if (@_started)
                    return
                end

                #mark shell as started
                @_started = true

                #check for commands folder
                if (kwargs.has_key?(:cmd_folder))
                    @_cmd_folder = kwargs[:cmd_folder]
                end

                while (true)
                    begin
                        #read user input
                        command = Readline.readline(@_identifier)
                        if (command == nil)
                            puts
                            next
                        end

                        #strip spaces
                        command = command.strip

                        ##check if command is empty, yes, continues to next user input
                        if (command.empty?)
                            next #continue to next loop iteration
                        end

                        #add command to history list
                        @_cmd_history.push({:cmd => command, :time => Time.new})
                        Readline::HISTORY.push(command)

                        ##split action and arguments
                        cmd_splitted = command.split(/\s+/)

                        #get action name and args
                        cmd = cmd_splitted[0].to_sym
                        args = cmd_splitted[1 .. -1]

                        #run command
                        self.run(cmd, *args)
                    rescue Interrupt => ex
                        puts
                        self.run(:exit)
                    rescue SystemExit => ex
                        raise(ex)
                    rescue Exception => ex
                        puts(ex.message)
                    end
                end
            end

            def run(cmd, *args)
                #convert cmd to string
                cmd = cmd.to_s

                #check if command is valid
                type = self._has_cmd?(cmd)
                if (!type)
                    self.run('default')
                    return nil
                end

                begin
                    if (type == :method)
                        self.send("_cmd_#{ cmd }".to_sym, *args)
                    else
                        #load command file and run
                        script = CommandScript.new(@_cmd_folder + '/' + cmd + '.rb')
                        script.main(self, *args)
                    end
                rescue SystemExit => ex
                    raise(ex)
                rescue Exception => ex
                    raise("'#{ cmd }' COMMAND ERROR: #{ ex.message } trace:\n\n#{ ex.backtrace.join("\n") }")
                end

                return nil
            end

            # Protected methods
            protected

            def _root()
                raise(NotImplementedError)
            end

            def _identifierr()
                return ">> "
            end

            #check if the command exists, and return the type of it: method or file
            def _has_cmd?(cmd)
                #lower case it
                cmd = cmd.downcase

                #check on cmd folder if setted
                if (@_cmd_folder)
                    cmd_file = @_cmd_folder + '/' + cmd + '.rb'
                    if (File.exists?(cmd_file))
                        return :file
                    end
                end

                #check method
                cmd_method = "_cmd_#{ cmd }".to_sym
                if (self.respond_to?(cmd_method, true))
                    return :method
                end

                return nil
            end

            def _save_history()
                begin
                    @_history.db.transaction do
                        @_cmd_history.each do |item|
                            #save to the database
                            cmd_db = @_history.Command.new(
                                :command  => item[:cmd],
                                :datetime => item[:time]
                            )

                            cmd_db.save
                        end
                    end
                rescue Exception => ex
                    puts("Error saving command history")
                end
            end

            def _load_history()
                #delete history 3 days old
                start_delete = Time.new - 259200
                @_history.Command.where('datetime <= ?', start_delete).delete

                #get commands available
                @_history.Command.all do |item|
                    Readline::HISTORY.push(item.command)
                end
            end


            #define default commands
            public

            self.add_command(:history) do |*args|
                list = Readline::HISTORY.to_a
                list.pop #remove last entry, it's the command 'history'

                #print list
                list.each do |item|
                    puts(item)
                end
            end

            self.add_command(:default) do |*args|
                puts("invalid command")
            end

            self.add_command(:quit) do |*args|
                self.run(:exit, *args)
            end

            self.add_command(:exit) do |*args|
                self._save_history
                exit
            end

            #clear screen
            self.add_command('clear') do |*args|
                system("clear")
            end

        end
    end
end

require_relative('shell/schema')
