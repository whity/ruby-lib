module Minds
    class I18n
        attr_accessor(:locale)

        #setup i18n module data
        def initialize(**kwargs)
            @locale = kwargs.delete(:locale)
            if (!@locale)
                raise(ArgumentError, "invalid locale")
            end

            #check for backend
            @_backend = kwargs[:backend] ? kwargs.delete(:backend) : :Yaml
            if (@_backend.kind_of?(String) || @_backend.kind_of?(Symbol))
                @_backend = Backend.create(@_backend, kwargs)
            elsif (!@_backend.kind_of?(Backend::Plugin))
                raise(ArgumentError, "invalid backend: #{ @_backend }")
            end

            return self
        end
        
        #available locales
        def available_locales()
            return @_backend.available_locales
        end

        #reload
        def reload()
            @_backend.load

            return self
        end
        
        def translate(key, **options)
            return self._process(**options) do |locale|
                result = @_backend.lookup(locale, key, **options)
                if (!result)
                    next key
                end

                if (!result.kind_of?(Hash))
                    next result
                end

                ###if result is a 'hash' and we passed 'count', check for the right message to return
                if (!options[:count])
                    next result['other']
                end

                count = options[:count].to_i

                #all keys 'number' or 'number..number' are a range, get the first that 'count' matches
                msg = result.delete('other')
                result.sort.each do |key, value|
                    match = key.match(/^\d+$/)
                    if (match && count == key.to_i)
                        msg = value
                        break
                    end

                    match = key.match(/^(\d+)..(\d+)?$/i)
                    if (match)
                        #if end isn't defined, just check if count is bigger or equal than start
                        if (!match[2])
                            if (count >= match[1].to_i)
                                msg = value
                                break
                            end

                            next
                        end
                        
                        range = Range.new(match[1].to_i, match[2].to_i)
                        if (range.member?(count))
                            msg = value
                            break
                        end 
                    end 
                end
                
                if (!msg)
                    next key + "(#{ count })"
                end

                next sprintf(msg, count)
            end
        end

        def date(value=nil, **options)
            return self._process(**options) do |locale|
                formats = @_backend.date(locale, **options)
                next self._date_time(value, locale, formats, **options)
            end
        end

        def time(value=nil, **options)
            return self._process(**options) do |locale|
                formats = @_backend.time(locale, **options)
                next self._date_time(value, locale, formats, **options)
            end
        end

        def currency(value=nil, **options)
            return self._process(**options) do |locale|
                #convert value to string
                value = value.to_s
                
                #get currency definitions from backend
                currency_formats = @_backend.currency(locale, **options)
                
                if (!value || value.length == 0)
                    next currency_formats
                end

                #format value to number
                value = self.number(value, **options)

                #get format
                format = currency_formats['format']

                #buid new value
                next sprintf(format, value)
            end
        end
        
        def number(value=nil, **options)
            return self._process(**options) do |locale|
                #convert value to string
                value = value.to_s

                #get number definitions from backend
                nbr_formats = @_backend.number(locale, **options)

                if (!value || value.length == 0)
                    next nbr_formats
                end

                #get decimal separator
                dec_separator = nbr_formats['decimal_separator']
                if (dec_separator)
                    value = value.sub(/\./, dec_separator)
                end
                
                ###set precision separator###
                #split by decimal separator
                match = value.match(/(\d+)#{ dec_separator }?(\d+)?/i)
                (integer, decimal) = match[1, 2]
                
                precision_separator = nbr_formats['precision_separator']
                new_value = ''
                counter = 0
                index = integer.length - 1
                while (index >= 0)
                    if (counter >= 3)
                        new_value = precision_separator + new_value
                        counter = 0 
                    end

                    new_value = integer[index] + new_value

                    index -= 1
                    counter += 1
                end
                
                value = new_value
                if (decimal)
                    value += dec_separator + decimal
                end
                ###

                next value
            end
        end

        # Protected methods
        protected

        def _process(**options, &block)
            locale = options[:locale] ? options.delete(:locale) : @locale 

            return block.call(locale)
        end

        def _date_time(value, locale, formats, **options)
            #value should respond to 'strftime'
            if (value != nil && !value.respond_to?(:strftime))
                raise(ArgumentError, "invalid value")
            end

            #get date definitions from backend
            if (value == nil)
                return formats
            end
            
            #get type to use
            type = options[:type] ? options[:type].to_s : 'default'
            if (!formats['formats'][type])
                raise(ArgumentError, "format '#{ type }' not defined")
            end

            #translate text value
            format = formats['formats'][type]

            #if the caller method was 'date', do the replace, otherwise ignore it
            if (caller[0] =~ /date/i)
                format = format.gsub(/%[aAbB]/) do |match|
                    replace = nil
                    if (match == '%a')
                        replace = formats['abbr_day_names'][value.wday]
                    elsif (match == '%A')
                        replace = formats['day_names'][value.wday]
                    elsif (match == '%b')
                        replace = formats['abbr_month_names'][value.month]
                    elsif (match == '%B')
                        replace = formats['month_names'][value.month]
                    end

                    if (replace)
                        next replace
                    end

                    next match
                end
            end

            return value.strftime(format)
        end
    end
end

require_relative( "i18n/backend" )
