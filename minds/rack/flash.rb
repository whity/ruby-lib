module Minds
    module Rack
       class Flash
            def initialize(rack_app, kwargs={})
                @_rack_app = rack_app
            end
            
            def call(env)
                env['rack.session.flash'] = Handler.new( env ) 

                return @_rack_app.call(env)
            end

            class Handler
                def initialize(env)
                    @_data = env['rack.session'][:__FLASH__]
                    if (@_data == nil)
                        @_data = Hash.new
                    end

                    @_next = Hash.new
                    env['rack.session'][:__FLASH__] = @_next
                end

                def []=(key, value)
                    @_next[key] = value
                    @_data[key] = value

                    return value
                end

                def [](key)
                    return @_data[key]
                end
            end
       end
    end
end
