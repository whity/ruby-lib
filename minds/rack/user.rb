module Minds
    module Rack
        class User
            def initialize( rack_app, kwargs={} )
                @_rack_app = rack_app
                @_options  = kwargs
            end
            
            def call( env )
                # get handler to instanciate
                handler = @_options[:handler] || Handler
                
                # instanciate user handler object
                env['user'] = handler.new( env )

                #run rack_app
                return @_rack_app.call( env )
            end

            class Handler
                # instance public attributes
                attr_reader( :username, :session )

                # constructor
                def initialize( env )
                    @_env = env
                    @_roles = nil

                    @session  = env['rack.session']
                    @username = env['rack.session.user']
                end

                # return bool, check's if user is authenticated
                def authenticated?()
                    if ( @username )
                        return true
                    end

                    return false
                end

                # logout user
                def logout()
                    # clear username
                    @username = @_env['rack.session.user'] = nil

                    # destroy session data
                    @session.clear

                    # force session renew
                    @_env['rack.session.options'][:renew] = true

                    return true
                end

                #return the user roles
                def roles()
                    if ( @_roles == nil )
                        @_roles = ['guest']
                    end

                    return @_roles.clone
                end

                # PROTECTED METHODS
                protected

                def _set_username( username )
                    @username = username
                    @_env['rack.session.user'] = username

                    return self
                end
            end
        end
    end
end
