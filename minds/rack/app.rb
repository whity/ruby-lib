module Minds
    module Rack
        class App
            def initialize( rack_app, app, kwargs={} )
                @_rack_app = rack_app
                @_app      = app
            end
            
            def call(env)
                env['app'] = @_app
                
                return @_rack_app.call( env )
            end
        end
    end
end
