require( 'rack/session/abstract/id' )
require( 'minds/pluggable' )

module Minds
    module Rack
        class Session < ::Rack::Session::Abstract::ID
            attr_reader( :mutex, :pool )

            DEFAULT_OPTIONS = ::Rack::Session::Abstract::ID::DEFAULT_OPTIONS.merge(
                :store => 'Redis',
            )

            def initialize( app, options={} )
                super( app, options )

                @mutex = Mutex.new
                @pool  = ::Minds::Rack::Session::Store.create( **@default_options )

                return self
            end

            def get_session( env, sid )
                return self.with_lock( env, [nil, {}] ) do
                    # session from redis
                    session = nil
                    if ( sid )
                        session = @pool.get( sid )
                    end

                    if ( session == nil )
                        session = {
                            'user' => nil,
                            'data' => {}
                        }

                        # create new sid
                        sid = generate_sid

                        # create new session on redis
                        @pool.set( sid, session )
                    end

                    env['rack.session.user'] = session['user']

                    next [sid, session['data']]
                end
            end

            def set_session( env, sid, session, options )
                return self.with_lock( env, false ) do
                    # get session
                    sess = @pool.get( sid )

                    if ( options[:renew] || options[:drop] )
                        # if valid session object, destroy
                        if ( sess )
                            @pool.del( sid )
                        end

                        # if is to drop, stop process
                        if ( options[:drop] )
                            next false
                        end

                        # generate new session id
                        sid = generate_sid

                        # create the session
                        sess = {'user' => nil, 'data' => {}}
                    end
                    
                    # set new session data
                    sess['data'] = session
                    sess['user'] = env['rack.session.user']

                    # save session
                    @pool.set( sid, sess, options )

                    next sid
                end
            end

            def destroy_session( env, sid, options )
                return self.with_lock( env ) do
                    # get session
                    sess = @pool.get( sid )

                    if (sess != nil)
                        # delete it
                        @pool.del( sid )
                    end

                    if ( options[:drop] )
                        next
                    end

                    next generate_sid
                end
            end

            def with_lock( env, default=nil )
                result = default

                begin
                    if ( env['rack.multithread'] )
                        @mutex.lock
                    end
                    result = yield
                ensure
                    if ( @mutex.locked? )
                        @mutex.unlock
                    end
                end

                return result
            end
        end
    end
end

require( 'minds/rack/session/store' )
