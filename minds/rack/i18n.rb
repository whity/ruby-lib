require( 'rack/request' )
require( 'rack/response' )

module Minds
    module Rack
        class I18n
            def initialize(rack_app, kwargs={})
                @_config = kwargs
                @_rack_app = rack_app
            end
            
            def call(env)
                env['i18n'] = Handler.new(env, @_config)

                #run rack_app
                (status, headers, body) = @_rack_app.call(env)

                #save lang cookie
                response = ::Rack::Response.new(body, status, headers) 
                response.set_cookie( 
                    'lang', 
                    { 
                        :value => env['i18n'].locale,
                        :path  => '/'
                    } 
                )

                return response.finish
            end

            class Handler
                attr_accessor(:locale)

                def initialize(env, kwargs={})
                    @_env = env

                    #check param 'l' or cookie lang
                    request = ::Rack::Request.new(@_env)
                    @locale = kwargs[:default]
                    if (request.params['l'])
                        @locale = request.params['l']
                    elsif (request.cookies['lang']) #check cookie 'lang'
                        @locale = request.cookies['lang']
                    end

                    return self
                end
                
                def method_missing(method, object, options={}, &block)
                    if (!options.has_key?(:locale))
                        options[:locale] = @locale
                    end

                    return @_env['app'].i18n.send(method.to_sym, object, options, &block)
                end
            end
        end
    end
end
