require( 'minds/pluggable' )

module Minds
    module Rack
        class Session
            module Store
                extend( ::Minds::Pluggable )
                
                class Plugin
                    # get session
                    def get( sid )
                        raise( NotImplementedError )
                    end

                    # set session
                    def set( sid )
                        raise( NotImplementedError )
                    end

                    # delete session
                    def del( sid )
                        raise( NotImplementedError )
                    end
                end

                #setup plugins
                _pluggable_setup( require: false )

                class << self
                    def create( **kwargs )
                        store_config = kwargs[:store].clone
                        store = self._pluggable_get( store_config.delete( :type ) )
                        if ( !store )
                            raise( ArgumentError, "invalid store '#{ store }'" )
                        end

                        return store.new( **store_config )
                    end
                end
            end
        end
    end
end
