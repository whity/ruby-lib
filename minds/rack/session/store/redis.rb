require( 'redis' )
require( 'minds/rack/session/store' )

module Minds
    module Rack
        class Session
            module Store
                class Redis < Plugin
                    def initialize( **kwargs )
                        config = kwargs

                        @_redis = ::Redis.new( config )

                        return self
                    end

                    def get( sid )
                        data = @_redis.get( self._fix_sid( sid ) )
                        if ( data != nil )
                            data = Marshal.load( data )
                        end

                        return data
                    end

                    def set( sid, data, options={} )
                        @_redis.set( 
                            self._fix_sid( sid ), 
                            Marshal.dump( data ), 
                            { :ex => options[:expire_after] } 
                        )

                        return self
                    end

                    def del( sid )
                        @_redis.del( self._fix_sid( sid ) )

                        return self
                    end

                    protected

                    def _fix_sid( sid )
                        return 'sessions:' + sid
                    end
                end
            end
        end
    end
end
