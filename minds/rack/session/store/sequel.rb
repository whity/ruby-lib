require( 'sequel' )
require( 'minds/rack/session/store' )

module Minds
    module Rack
        class Session
            module Store
                class Sequel < Plugin
                    @@_db    = nil
                    @@_model = nil

                    def initialize( **kwargs )
                        config = kwargs

                        # connect to database
                        if ( @@_db == nil )
                            @@_db = ::Sequel.connect( @config[:dsn] )
                        end

                        #check if db already has the table
                        if ( !@@_db.table_exists?( :tb_sessions ) )
                            @@_db.create_table( :tb_sessions ) do
                                primary_key( :id )
                                column( :sid, :String, :unique => true )
                                column( :data, :Blob )
                                column( :user, :String )
                                column( :expires, :String )
                            end
                        end

                        # model
                        if ( @@_model == nil )
                            @@_model = Class.new( ::Sequel::Model( :tb_sessions ) ) do
                                plugin( :serialization, :marshal, :data )
                                serialize_attributes( :marshal, :data ) 
                            end
                        end

                        return self
                    end

                    def get( sid )
                        data    = nil
                        session = @@_model.first(
                            'sid = :sid and expires > :dt_current',
                            :sid        => sid, 
                            :dt_current => Time.now.strftime( '%Y-%m-%d %H:%M:%S' )
                        )

                        if ( session != nil )
                            data = {
                                'user' => session.user
                                'data' => session.data
                            }
                        end

                        return data
                    end

                    def set( sid, data, options={} )
                        # clone data
                        data = ::Minds::Utils::Object.deep_clone( data )

                        # get from database
                        session = @@_model.first( :sid => sid )
                        if ( session == nil )
                            session = @@_model.new( :sid => sid, :data => Hash.new )
                        end
                        
                        # calculate expires, if not present yet
                        expires = options[:expires]
                        if ( options[:expire_after] && !expires )
                            expires = Time.now + options[:expire_after]
                        end

                        session.user    = data.delete( 'user' )
                        session.data    = data
                        session.expires = expires.strftime( '%Y-%m-%d %H:%M:%S' )
                        
                        return self
                    end

                    def del( sid )
                        session = @@_model.first( :sid => sid )
                        if ( session != nil )
                            session.destroy
                        end

                        return self
                    end
                end
            end
        end
    end
end
