#encoding: utf-8

require( 'minitest/autorun' )
require( 'minds/data/form' )

class Form < Minds::Data::Form
    _field( :name => 'field', :validators => [:NotEmpty] )
end

class ConfigDataFormTest < MiniTest::Unit::TestCase
    def setup()
        @form = Form.new
        @data = { 'field' => 2 }

        return self
    end
    
    def test_field()
        return assert_kind_of( ::Minds::Data::Form::Field, @form.field )
    end

    def test_set_data()
        @form.data = @data.clone
        return assert_equal( @data['field'], @form.field.value )
    end

    def test_get_data()
        @form.data = @data.clone
        return assert_equal( @data, @form.data )
    end

    def test_dynamic_field()
        @form.add_field( :name => 'field2', :validators => [:NotEmpty] )
        return assert_kind_of( ::Minds::Data::Form::Field, @form.field2 )
    end

    def test_validate_with_errors()
        return assert( @form.validate )
    end

    def test_validate_no_errors()
        @form.field.value = 2        
    
        return assert_nil( @form.validate )
    end
end
