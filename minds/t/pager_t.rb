#encoding: utf-8

require('minitest/autorun')
require('minds/data/pager')

class DataPagerTest < MiniTest::Unit::TestCase
    def setup()
        @total_entries = 50
        @per_page = 8
        @current_page = 1
        @last_page = (@total_entries / @per_page.to_f).ceil

        @pager = Minds::Data::Pager.new(
            @total_entries, 
            @per_page, 
            @current_page,
            :interval => 5
        )

        return
    end

    def test_last_page()
        return assert_equal(@last_page, @pager.last_page)
    end

    def test_previous_page()
        prev_page = @current_page - 1
        if (prev_page <= 0)
            prev_page = 1
        end

        return assert_equal(prev_page, @pager.previous_page)
    end

    def test_next_page()
        next_page = @current_page + 1
        if (next_page > @last_page)
            next_page = @last_page
        end

        return assert_equal(next_page, @pager.next_page)
    end

    def test_pages()
        return assert_equal([1, 2, 3, 4, 5], @pager.pages)
    end
end
