#encoding: utf-8

require( 'minitest/autorun' )
require( 'minds/utils/array/filter' )

class ArrayFilterTest < MiniTest::Unit::TestCase
    def setup()
        @array  = [
            {:name => 'xpty', :age => 10, :job => 'e'},
            {:name => 'xpto', :age => 23, :job => 'w'},
            {:name => 'xpta', :age => 25, :job => 'y'},
            {:name => 'xptz', :age => 30, :job => 'x'},
            {:name => 'xptb', :age => 50, :job => 'b'},
            {:name => 'zzz',  :age => 50, :job => 'a'},
            {:name => 'zzz',  :age => 10, :job => 'a'},
        ]

        return
    end
    
    def mdl
        return ::Minds::Utils::Array
    end

    def test_bigger_than()
        items = @array.select do |item|
            if (item[:age] > 50)
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :age => {'>' => 50} ) )
    end

    def test_equal_to()
        items = @array.select do |item|
            if (item[:age] == 10)
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :age => 10 ) )
    end

    def test_bigger_equal_than()
        items = @array.select do |item|
            if (item[:age] >= 25)
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :age => {'>=' => 25} ) )
    end

    def test_less_equal_than()
        items = @array.select do |item|
            if (item[:age] <= 23)
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :age => {'<=' => 23} ) )
    end

    def test_in()
        ages = [50, 30, 10]
        items = @array.select do |item|
            if (ages.include?(item[:age]))
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :age => {'in' => ages} ) )
    end

    def test_regexp()
        re = /^xpt/i
        items = @array.select do |item|
            if (item[:name].match(re))
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :name => {'=~' => re} ) )
    end

    def test_or()
        items = @array.select do |item|
            if (item[:name] == 'xptb' || item[:name] == 'zzz')
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, "-or" => [ {:name => 'xptb'}, {:name => 'zzz'} ] ) )
    end

    def test_explicit_and()
        name_re = /^xpt/i
        items = @array.select do |item|
            if (item[:name].match(name_re) && item[:age] == 50)
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, "-and" => [{:name => {'=~' => name_re}}, {:age => 50}] ) )
    end

    def test_implicit_and()
        name_re = /^xpt/i
        items = @array.select do |item|
            if (item[:name].match(name_re) && item[:age] == 50)
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :name => {'=~' => name_re}, :age => 50 ) )
    end

    def test_and_or()
        items = @array.select do |item|
            if (item[:name] == 'zzz' && (item[:age] == 10 || item[:age] >= 50))
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :name => 'zzz', '-or' => [ {:age => 10}, {:age => {'>=' => 50}} ] ) )
    end

    def test_and_or2()
        items = @array.select do |item|
            if (item[:name] == 'zzz' && (item[:age] == 10 || item[:job] == 'e'))
                next true
            end

            next false
        end

        return assert_equal( items, self.mdl.filter( @array, :name => 'zzz', '-or' => [:age => 10, :job => 'e'] ) )
    end
end
