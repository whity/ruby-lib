#encoding: utf-8

require( 'minitest/autorun' )
require( 'minds/utils/array/sort' )

class ArrayFilterTest < MiniTest::Unit::TestCase
    def setup()
        return @array = [
            {:name => 'xpty', :age => 10, :job => 'e'},
            {:name => 'xpto', :age => 23, :job => 'w'},
            {:name => 'xpta', :age => 25, :job => 'y'},
            {:name => 'xptz', :age => 30, :job => 'x'},
            {:name => 'xptb', :age => 50, :job => 'b'},
            {:name => 'zzz',  :age => 50, :job => 'a'},
            {:name => 'zzz',  :age => 10, :job => 'a'},
        ]
    end

    def test_single_column_asc()
        items = @array.sort { |a, b| a[:age] <=> b[:age] }

        return assert_equal( items, ::Minds::Utils::Array.sort( @array, 'age asc' ) )
    end
end
