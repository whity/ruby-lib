#encoding: utf-8

require('minitest/autorun')
require('minds/pluggable')
require('minds/utils/array/sort')

class UtilsArraySortTest < MiniTest::Unit::TestCase
    def setup()
        return @array = [
            {:name => 'xpty', :age => 10, :job => 'e'},
            {:name => 'xpto', :age => 23, :job => 'w'},
            {:name => 'xpta', :age => 25, :job => 'y'},
            {:name => 'xptz', :age => 30, :job => 'x'},
            {:name => 'xptb', :age => 50, :job => 'b'},
            {:name => 'zzz',  :age => 50, :job => 'a'},
            {:name => 'zzz',  :age => 10, :job => 'a'},
        ]
    end

    def test_single_column_explicit_asc()
        items = @array.sort do |a, b|
            a[:name] <=> b[:name]
        end

        return assert_equal(items, ::Minds::Utils::Array.sort(@array, "name asc"))
    end

    def test_single_column_implicit_asc()
        items = @array.sort do |a, b|
            a[:name] <=> b[:name]
        end

        return assert_equal(items, ::Minds::Utils::Array.sort(@array, "name"))
    end

    def test_multiple_columns()
        items = @array.sort do |a, b|
            if (a[:name] < b[:name])
                next -1
             elsif (a[:name] > b[:name])
                 next 1
            end

            #if the names are equal continue testing the other columns

            if (a[:age] <= b[:age])
                next -1
            else
                next 1
            end

            #equal
            next 0
        end

        return assert_equal(items, ::Minds::Utils::Array.sort(@array, "name, age"))
    end
end
