#encoding: utf-8

require('pp')
require('minitest/autorun')
require('minds/utils/hash/symbolize_keys')

class UtilsHashTest < MiniTest::Unit::TestCase
    def setup()
        @hash = {
            'test'  => 1,
            'test2' => 2,
            'test3' => 3,
            'test4' => {
                'test4.1' => 1,
                'test4.2' => 2
            }
        }

        return
    end
    
    def test_order()
        return :alpha
    end

    def test_01_symbolize_keys()
        sym_keys = @hash.keys.map { |item| item.to_sym }.sort
        new_hash = ::Minds::Utils::Hash.symbolize_keys(@hash)

        return assert_equal(sym_keys, new_hash.keys.sort)
    end
end
