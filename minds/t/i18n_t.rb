#encoding: utf-8

require('minitest/autorun')
require('yaml')
require('yaml/store')
require('minds/i18n')

class I18nTest < MiniTest::Unit::TestCase
    def setup()
        @folder = File.dirname( File.expand_path( __FILE__ ) ) + "/i18n_t"
        @i18n = Minds::I18n.new(
            :locale => 'pt', #default locale
            :folder => @folder 
        )

        return
    end

    def test_available_locales()
       return assert_equal(['pt', 'en'].sort, @i18n.available_locales.sort)
    end

    def test_simple_translation()
       return assert_equal("olá", @i18n.translate("hello"))
    end

    def test_pluralization_other()
        return assert_equal("sem mensagens", @i18n.translate("new_message", :count => 0))
    end

    def test_pluralization_one()
        return assert_equal("tem uma nova mensagem", @i18n.translate("new_message", :count => 1))
    end

    def test_pluralization_more_than_one()
        return assert_equal("tem 3 novas mensagens", @i18n.translate("new_message", :count => 3))
    end

    def test_format_small_number()
        return assert_equal("1,23", @i18n.number(1.23))
    end

    def test_format_mid_number()
        return assert_equal("23.457", @i18n.number(23457))
    end

    def test_format_big_number()
        return assert_equal("1.234.457", @i18n.number(1234457))
    end

    def test_format_short_date()
        return assert_match(/\d{4}-\d{2}-\d{2}/, @i18n.date(Time.now))
    end

    def test_format_long_date()
        return assert(@i18n.date(Time.now))
    end

    def test_format_time()
        return assert_match(/\d{1,2}:\d{1,2}(:\d{1,2})?\s*([ap]m)?/, @i18n.time(Time.now))
    end

    #test reload (backend.load)
    def test_reload()
        #create 'fr' locale
        filename = @folder + '/fr.yml'
        store = ::YAML::Store.new( filename )
        store.transaction do
            store['fr'] = {
                'hello' => 'bonjour'
            }
        end
        store = nil

        #reload i18n
        @i18n.reload

        #test
        begin
            assert_equal( 'bonjour', @i18n.translate( 'hello', :locale => 'fr' ) )
        ensure
            #delete the 'fr' translation file
            File.delete( filename )
        end

        return
    end
end
