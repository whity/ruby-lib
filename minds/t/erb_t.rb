#encoding: utf-8

require( 'minitest/autorun' )
require( 'minds/erb' )

class ErbTest < MiniTest::Unit::TestCase
    def setup()
        @template = Minds::ERB.new( 'hello <%= name %>' )
        @str      = 'hello world'
    end

    def test()
        return assert_equal( @str, @template.result( :name => 'world' ) )
    end
end
