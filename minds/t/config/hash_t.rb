#encoding: utf-8

require( 'minitest/autorun' )
require( 'minds/config' )
require( 'minds/utils/object/deep_clone' )

class ConfigHashTest < MiniTest::Unit::TestCase
    def setup()
        @file      = File.dirname(File.expand_path(__FILE__)) + "/test.yaml"
        @file_data = ::YAML.load_file(@file)
        @config    = Minds::Config.create(:type => :Hash, :data => @file_data)

        return
    end

    def test_01_simple_key()
        return assert_equal(@file_data['scalar'], @config['scalar'])
    end

    def test_02_sub_key()
        return assert_equal(@file_data['hash']['first'], @config['hash.first'])
    end

    def test_03_sub_sub_key()
        return assert_equal(
            @file_data['hash_hash' ]['first']['first_first'],
            @config['hash_hash.first.first_first']
        )
    end
end
