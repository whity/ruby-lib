require( 'erb' )

module Minds
    class ERB < ::ERB
        class Data
            def initialize(**kwargs)
                # for each key in hash create a instance variable
                kwargs.each do |key, value|
                    var_name = "@#{ key }".to_sym
                    self.instance_variable_set(var_name, value)
                    self.class.send(:attr_reader, key)
                end
            end
            
            def get_binding()
                return binding
            end
        end
        
        def result(**kwargs)
            data = Data.new(**kwargs)
            return super(data.get_binding)
        end
    end
end
