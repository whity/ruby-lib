module Minds
    module Redcarpet
        class CodeRayify < ::Redcarpet::Render::HTML
            def block_code(code, language)
                language ||= :plaintext

                return ::CodeRay.scan(code, language).div
            end
        end
    end
end
