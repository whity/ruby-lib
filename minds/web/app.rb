require( 'singleton' )
require( 'sprockets' )
require( 'yui/compressor' )
require( 'thin' )

require( 'minds/app' )

module Minds
    module Web
        class App < Minds::App
            include( Singleton )

            def initialize( **kwargs )
                @_env = kwargs[:env]
                if (!@_env)
                    @_env = ENV.has_key?('MINDS_ENV') ? ENV['MINDS_ENV'] : :development
                end
                @_env = @_env.to_sym

                #set rack_env
                ENV['RACK_ENV'] = @_env.to_s

                return super()
            end

            def env()
                return @_env
            end

            def root()
                raise( Exception, "must be overwritten" )
            end

            def build( rack, kwargs={} )
                #build assets
                self._build_assets

                #load controllers
                self._load_controllers

                #map controllers
                self._map_controllers( rack )

                return self
            end

            def logger()
                raise( NotImplementedError )
            end
            
            def run( options={} )
                rack_app = self.build

                # run rack app
                ::Rack::Handler::Thin.run(
                    rack_app,
                    ::Minds::Utils::Hash.symbolize_keys( options )
                )
                
                return
            end

            class << self
                #check method on application instance
                def method_missing(meth, *args, &block)
                    return self.instance.send(meth.to_sym, *args, &block)
                end
            end

            # Protected methods
            protected

            def _load_controllers(dir=nil)
                if (dir == nil)
                    dir = self.root + '/controllers'
                end

                #open dir
                dir = Dir.new(dir)

                #process dir items
                dir.each do |item|
                    if (['..', '.'].index(item) != nil)
                        next
                    end

                    filename = dir.path + '/' + item

                    #if it's a file and doesn't has the extension 'rb' continue to next
                    if (File.file?(filename) && filename.match(/\.rb$/i))
                        #require controller
                        require(dir.path + '/' + item)
                    elsif (File.directory?(filename))
                        self._load_controllers(filename)
                    end
                end

                #close dir
                dir.close

                return
            end

            def _map_controllers(rack, mod=nil)
                if (mod == nil)
                    mod = self.class::Controllers
                end

                controllers = mod.constants
                controllers.each do |item|
                    item = mod.const_get(item)
                    if (item.class == Class && item.mountable? && item.mount_point)
                        rack.map(item.mount_point) do
                            run(item.new)
                        end
                    elsif (item.class == Module)
                        self._map_controllers(rack, item)
                    end
                end
            end

            def _build_assets()
                src_dir    = self.root + '/assets'
                output_dir = self.root + '/public/static'

                # if directory assets doesn't exist don't do nothing
                if ( !Dir.exist?( src_dir ) )
                    return
                end

                self.logger.info( "building assets..." )

                ### init sprockets environment
                environment = Sprockets::Environment.new
                
                # config sprockets environment 
                self._build_assets_sprockets_config( environment )

                process_type = lambda do |type|
                    #folder path
                    folder = "#{ src_dir }/#{ type }"

                    #get list of files in folder
                    files = Dir.glob(folder + "/*." + type)

                    #create the type output dir if doesn't exist
                    output_folder = output_dir + '/' + type
                    if (!Dir.exist?(output_folder))
                        Dir.mkdir(output_folder)
                    end

                    #add folder to sprockets environment
                    environment.append_path(folder)

                    #write each file to 'app/public/#{ type }/file.#{ type }'
                    files.each do |filename|
                        only_filename = filename.match(/\/([^\/]+)$/i)[1]
                        output_file = output_folder + '/' + only_filename

                        # write file
                        File.open( output_file, 'w' ) do |file|
                            file.puts( environment[only_filename].source )
                        end
                    end

                    return
                end

                #process each folder in assets
                Dir.entries(src_dir).each do |item|
                    if ( item[0] == '.' )
                        next
                    end

                    process_type.call(item)
                end

                return
            end

            def _build_assets_sprockets_config( environment )
                # only compress js and css in production environment
                if ( self.env == :production )
                    environment.js_compressor  = ::YUI::JavaScriptCompressor.new
                    environment.css_compressor = ::YUI::CssCompressor.new
                end
                ###
            end
        end
    end
end
